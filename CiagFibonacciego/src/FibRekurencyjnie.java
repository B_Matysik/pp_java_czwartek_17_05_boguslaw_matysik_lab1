/**
 * Created by bogus on 09.03.2017.
 */
public class FibRekurencyjnie {

    public long fibrLicz(int n){
        //System.out.println("    n: "+n); //test ogromna ilość wywołań funkcji
        if(n<=1) return n;
        else return fibrLicz(n-1)+fibrLicz(n-2);
    }
    public long suma(long max){
        long s=0, n=0;
        int i=1;
        while(n<=max){
            n=fibrLicz(i);
            if(n%2==0) s=s+n;
            i++;
        }
        return s;
    }
}
