import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by bogus on 09.03.2017.
 */
public class Licz  {
    int liczba =100;
    long tabLiczbTrojkatnych[] = new long[liczba];
    // Generowanie tablicy liczb trojkątnych
    private void genTablicy(){
        for (int i=0; i<liczba; i++){
            tabLiczbTrojkatnych[i] = (i*(i+1))/2;
        }
        /*for (int i=0; i<liczba; i++){
            System.out.println(i+". "+ tabLiczbTrojkatnych[i]);
        }*/
    }


    public void licz() throws FileNotFoundException {
        File plik = new File("C:\\Users\\bogus\\IdeaProjects\\p042_words.txt");
        Scanner in = new Scanner(plik).useDelimiter("[^a-zA-Z]+");  //plusik  na końcu oznacza że string musi miec co najmiej 1 znak
        genTablicy();

        int j=0;
        int nWyrazowTrojkatnych=0;
        int maxL=0;

        while (in.hasNext()) {
            String zdanie = in.next();
            /*int i=0;
            System.out.println(i+". "+zdanie);
            i++;*/
            // Liczenie wartości liter w słowie
            char[] chars = zdanie.toCharArray();
            int sumLiter=0;
            for (int i = 0; i<chars.length; i++){
                char c = chars[i];
                sumLiter=sumLiter+(c-64);
                //System.out.println(c+" ma wartość: "+(int)c+" i jest: "+(c-64)+" literą w alfabecie");
            }
            nWyrazowTrojkatnych=nWyrazowTrojkatnych+czyTrojkatna(sumLiter);
            //System.out.println("suma liter wynosi: "+sumLiter);
            //System.out.println("    Suma wyrazów trojkatnych wynosi: "+nWyrazowTrojkatnych);
        }
        System.out.println("    Suma wyrazów trojkatnych wynosi: "+nWyrazowTrojkatnych);
    }
    private int czyTrojkatna(int sumaZnakow){
        for(int i=0; i<tabLiczbTrojkatnych.length; i++){
            if(sumaZnakow==tabLiczbTrojkatnych[i])
                return 1;
        }
        return 0;
    }
}
