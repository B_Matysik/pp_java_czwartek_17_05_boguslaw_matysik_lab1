/**
 * Created by bogus on 08.03.2017.
 */
import java.util.Scanner;
public class CiagFibonacciego {
    public static void main(String[] args) {
        long max = 4000000;
        Scanner read = new Scanner(System.in); //obiekt do odbierania danych
        System.out.println("To jest program który wylicza sumę parzystych wyrazów ciągu fibonacciego których wartość nie przekracza: "+max);
        System.out.println("podaj TWOJĄ max wartść liczby ciągu która ma nie zostać przekroczona: ");
        //max=read.nextLong();
        Fib fib = new Fib();
        fib.fibLicz(max);
        System.out.println("Liczba wyrazów: "+ fib.getLiczbaWyrazow());
        System.out.println("Liczba wyrazów parzystych: "+fib.getLiczbaWyrazowParzystych());
        System.out.println("Suma wyrazów parzystych: "+fib.getSumaWyrazowParzystych());
        //System.out.println("Suma wyrazów parzystych: "+fib.fibLicz(max));

        //wyraz Fibonacciego liczony rekurencyjnie
        FibRekurencyjnie fibr = new FibRekurencyjnie();
        System.out.println("Suma wyrazów parzystych dla ciągu liczonego rekurencyjnie wyniosła: "+fibr.suma(max));

    }
}
