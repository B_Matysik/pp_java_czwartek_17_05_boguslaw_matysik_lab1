/**
 * Created by bogus on 09.03.2017.
 */
public class Fib {
    private long x1=0, x2=1, temp, suma=0;
    private int n=0, n2=0;
    long fibLicz(long max){
        while(x1<max) {
            temp = x2;
            x2 = (x1 + x2);
            x1 = temp;
            n++;
            //System.out.println(n+". wartosci po iteracji x2="+x2+" x1="+x1);
            if (x2 % 2 == 0) {
                n2++;
                suma = suma + x2;
                System.out.println("    " + x2);
            }
        }
        return suma;
    }
    int getLiczbaWyrazow(){
        return n;}
    int getLiczbaWyrazowParzystych(){return n2;}
    long getSumaWyrazowParzystych(){return suma;}
}
